import { Component, OnInit, ChangeDetectionStrategy, Input, Inject } from '@angular/core';
import Swal from 'sweetalert2'


@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CardComponent implements OnInit {

  @Input() food : any


  constructor( 
   // private toastr: ToastrService,
   
  

  ) { }

  ngOnInit(): void {
  }

sendOneCommande(){
  // this.showSuccess(" Ajout Fait avec succes")
  Swal.fire({
    position: 'top-end',
    icon: 'success',
    title: 'Ajout fait avec succes',
    showConfirmButton: false,
    timer: 1500
  })
  
  
}
//  showSuccess(data: any) {
//       this.toastr.success(data);
//   }

//   showErreur(data : any) {
//       this.toastr.error(data);
//   }
}
