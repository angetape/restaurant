import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MenuRoutingModule } from './menu-routing.module';
import { MenuComponent } from './menu.component';
import { PlatEntreeComponent } from './components/plat-entree/plat-entree.component';
import { PlatDessertComponent } from './components/plat-dessert/plat-dessert.component';
import { PlatResistenceComponent } from './components/plat-resistence/plat-resistence.component';
import { AccueilComponent } from './components/accueil/accueil.component';
import { HeardersComponent } from './layouts/hearders/hearders.component';
import { NavBarComponent } from './layouts/nav-bar/nav-bar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import {MatIconModule} from '@angular/material/icon';
import { CommandeComponent } from './components/commande/commande.component'
import { MatButtonModule } from '@angular/material/button';
import { MatPaginatorModule } from '@angular/material/paginator';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
@NgModule({
  declarations: [
    MenuComponent,
    PlatEntreeComponent,
    PlatDessertComponent,
    PlatResistenceComponent,
    AccueilComponent,
    HeardersComponent,
    NavBarComponent,
    FooterComponent,
    CommandeComponent
  ],
  imports: [
    CommonModule,
    MenuRoutingModule,
    SharedModule,
    RouterModule,
    MatIconModule,
    MatInputModule,
    FormsModule,
    MatPaginatorModule,
    MatButtonModule,
    MatTableModule
  
  ]
})
export class MenuModule { }
