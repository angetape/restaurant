import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AccueilComponent } from './components/accueil/accueil.component';
import { CommandeComponent } from './components/commande/commande.component';
import { PlatDessertComponent } from './components/plat-dessert/plat-dessert.component';
import { PlatEntreeComponent } from './components/plat-entree/plat-entree.component';
import { PlatResistenceComponent } from './components/plat-resistence/plat-resistence.component';
import { MenuComponent } from './menu.component';

const routes: Routes = [
  {
    path: '',
    component: MenuComponent,
    children: [
      {
        path: 'accueil',
        component: AccueilComponent
      },
      {
        path: 'plat_desert',
        component: PlatDessertComponent,
      },
      {
        path: 'plat-entre',
        component: PlatEntreeComponent
      },
      {
        path: 'plat-resistance',
        component: PlatResistenceComponent
      },
      {
        path: 'commande',
        component: CommandeComponent
      }
    ],
   
  },
 
  // {
  //   path: '**',
  //   redirectTo: 'accueil',
  //   pathMatch: 'full'
  // }



];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MenuRoutingModule { }
