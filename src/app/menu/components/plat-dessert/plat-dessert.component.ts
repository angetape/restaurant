import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-plat-dessert',
  templateUrl: './plat-dessert.component.html',
  styleUrls: ['./plat-dessert.component.scss']
})
export class PlatDessertComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  dessert = [
    {
      image: ' ./assets/dessert/pexels-alexander-cuelove-2273823.jpg',
      description: 'The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan .    ',
      price: '20'
    },
    {
      image: ' ./assets/dessert/pexels-donald-tong-2205270.jpg',
      description: 'The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan .    ',
      price: '20'
    },
    {
      image: ' ./assets/dessert/pexels-na-urchin-2377173.jpg',
      description: 'The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan .    ',
      price: '20'
    },
    {
      image: ' ./assets/dessert/pexels-suzy-hazelwood-1126359.jpg',
      description: 'The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan .    ',
      price: '20'
    },
    {
      image: ' ./assets/dessert/pexels-alexander-cuelove-2273823.jpg',
      description: 'The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan .    ',
      price: '20'
    },
    {
      image: ' ./assets/dessert/pexels-donald-tong-2205270.jpg',
      description: 'The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan .    ',
      price: '20'
    },
    {
      image: ' ./assets/dessert/pexels-na-urchin-2377173.jpg',
      description: 'The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan .    ',
      price: '20'
    },
    {
      image: ' ./assets/dessert/pexels-suzy-hazelwood-1126359.jpg',
      description: 'The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan .    ',
      price: '20'
    },
  ]

}
