import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlatDessertComponent } from './plat-dessert.component';

describe('PlatDessertComponent', () => {
  let component: PlatDessertComponent;
  let fixture: ComponentFixture<PlatDessertComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlatDessertComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlatDessertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
