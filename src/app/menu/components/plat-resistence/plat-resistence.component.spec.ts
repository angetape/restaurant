import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlatResistenceComponent } from './plat-resistence.component';

describe('PlatResistenceComponent', () => {
  let component: PlatResistenceComponent;
  let fixture: ComponentFixture<PlatResistenceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlatResistenceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlatResistenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
