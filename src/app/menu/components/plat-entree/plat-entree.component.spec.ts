import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlatEntreeComponent } from './plat-entree.component';

describe('PlatEntreeComponent', () => {
  let component: PlatEntreeComponent;
  let fixture: ComponentFixture<PlatEntreeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlatEntreeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlatEntreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
