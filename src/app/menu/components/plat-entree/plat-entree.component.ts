import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-plat-entree',
  templateUrl: './plat-entree.component.html',
  styleUrls: ['./plat-entree.component.scss']
})
export class PlatEntreeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  fisrtPlat = [
    {
      image: ' ./assets/entrée/pexels-chan-walrus-1059905.jpg',
      description: 'The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan .   ',
      price: '20'
    },
    {
      image: ' ./assets/entrée/pexels-cats-coming-406152.jpg',
      description: 'The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan .   ',
      price: '20'
    },
    {
      image: ' ./assets/entrée/pexels-antony-trivet-13534421.jpg',
      description: 'The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan .   ',
      price: '20'
    },
    {
      image: ' ./assets/entrée/pexels-cats-coming-406152.jpg',
      description: 'The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan .   ',
      price: '20'
    },
    {
      image: ' ./assets/entrée/pexels-foodie-factor-551997.jpg',
      description: 'The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan .   ',
      price: '20'
    },
    {
      image: ' ./assets/entrée/pexels-chan-walrus-1059905.jpg',
      description: 'The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan .   ',
      price: '20'
    },
    {
      image: ' ./assets/entrée/pexels-cats-coming-406152.jpg',
      description: 'The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan .   ',
      price: '20'
    },
    {
      image: ' ./assets/entrée/pexels-chan-walrus-1059905.jpg',
      description: 'The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan .   ',
      price: '20'
    }

  ]

}
