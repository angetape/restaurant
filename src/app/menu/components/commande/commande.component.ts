import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-commande',
  templateUrl: './commande.component.html',
  styleUrls: ['./commande.component.scss'],
})
export class CommandeComponent implements OnInit {
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  public dataSource = new MatTableDataSource<any>();

  constructor() {}

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource<any>(this.dessert)
  }
  numberChoice = new FormControl('', [Validators.required]);

  dessert = [
    {
      image: ' ./assets/dessert/pexels-alexander-cuelove-2273823.jpg',
      description:
        'The Shiba Inu is the smallest  ...',
      price: '20',
    },
    {
      image: ' ./assets/dessert/pexels-donald-tong-2205270.jpg',
      description:
        'The Shiba Inu is the smallest  ...',
      price: '20',
    },
    {
      image: ' ./assets/entrée/pexels-chan-walrus-1059905.jpg',
      description:
        'The Shiba Inu is the smallest .   ',
      price: '20',
    },
    {
      image: ' ./assets/entrée/pexels-cats-coming-406152.jpg',
      description:
        'The Shiba Inu is the smallest ',
      price: '20',
    },
    {
      image: ' ./assets/image plat/img1.jpeg',
      description:
        'The Shiba Inu is the smallest',
      price: '20',
    },
    {
      image: ' ./assets/image plat/img2.jpeg',
      description:
        'The Shiba Inu is the smallest ',
      price: '20',
    },
  ];

 

 
}
