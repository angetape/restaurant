import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';

const routes: Routes = [


  { path: 'menu', loadChildren: () => import('./menu/menu.module').then(m => m.MenuModule) },
  {
    path: '**',
    redirectTo : 'menu',
    pathMatch: 'full'
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
